jQuery.sap.declare("exportcompliancescheduleb_listreport.Component");
sap.ui.getCore().loadLibrary("sap.ui.generic.app");
jQuery.sap.require("sap.ui.generic.app.AppComponent");

sap.ui.generic.app.AppComponent.extend("exportcompliancescheduleb_listreport.Component", {
	metadata: {
		"manifest": "json"
	}
});