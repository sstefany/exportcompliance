jQuery.sap.declare("exportcompliance.ExportCompliance_ALP.Component");
sap.ui.getCore().loadLibrary("sap.ui.generic.app");
jQuery.sap.require("sap.ui.generic.app.AppComponent");

sap.ui.generic.app.AppComponent.extend("exportcompliance.ExportCompliance_ALP.Component", {
	metadata: {
		"manifest": "json"
	}
});