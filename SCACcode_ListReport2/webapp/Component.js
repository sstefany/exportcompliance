jQuery.sap.declare("exportcompliancescaccode_listreport2.Component");
sap.ui.getCore().loadLibrary("sap.ui.generic.app");
jQuery.sap.require("sap.ui.generic.app.AppComponent");

sap.ui.generic.app.AppComponent.extend("exportcompliancescaccode_listreport2.Component", {
	metadata: {
		"manifest": "json"
	}
});