jQuery.sap.declare("exportcompliancelocation_listreport.Component");
sap.ui.getCore().loadLibrary("sap.ui.generic.app");
jQuery.sap.require("sap.ui.generic.app.AppComponent");

sap.ui.generic.app.AppComponent.extend("exportcompliancelocation_listreport.Component", {
	metadata: {
		"manifest": "json"
	}
});