jQuery.sap.declare("exportcomplianceiata_listreport2.Component");
sap.ui.getCore().loadLibrary("sap.ui.generic.app");
jQuery.sap.require("sap.ui.generic.app.AppComponent");

sap.ui.generic.app.AppComponent.extend("exportcomplianceiata_listreport2.Component", {
	metadata: {
		"manifest": "json"
	}
});